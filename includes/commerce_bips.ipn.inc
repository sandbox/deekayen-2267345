<?php

/**
 * @file
 * Verify IPN updates from BIPS and update relevant transactions.
 */

/**
 * Interpret IPN updates from BIPS and update relevant payment transactions.
 */
function commerce_bips_verify_ipn($order, $transaction) {
  watchdog('commerce_bips', 'IPN received for order @order_id, transaction @trans_id.', array('@trans_id' => $transaction->transaction_id, '@order_id' => $order->order_id), WATCHDOG_DEBUG, l(t('view order payments'), 'admin/commerce/orders/' . $order->order_id . '/payment'));

  $hash = hash('sha512', $_POST['transaction']['hash'] . $transaction->data['secret']);
  if ($_POST['hash'] == $hash && $_POST['status'] == 1) {
    $transaction->message = t('Transaction complete.');
    $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    $transaction->remote_status = $coinbase_transaction->status;
    $transaction->payload = $coinbase_transaction;
    commerce_payment_transaction_save($transaction);
    commerce_checkout_complete($order);
  }
  else {

  }
}
