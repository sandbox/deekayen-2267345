<?php

/**
 * @file
 * Callbacks used by hook_commerce_payment_method_info().
 */

/**
 * Global, required API configuration form.
 *
 * @return array
 *   Returns form elements for the payment method’s settings form included as
 *   part of the payment method’s enabling action in Rules.
 */
function commerce_bips_settings_form($settings = array()) {
  // Merge default settings into the stored settings array.
  $settings = (array) $settings + commerce_bips_default_settings();

  $form['apikey'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['apikey'],
    '#title' => t('BIPS API key'),
    '#size' => 45,
    '#maxlength' => 130,
    '#description' => t('BIPS API access key. Something like aW4x5kLr4mer4fovDJLGTMXSATkf81DLKcm349ajd12'),
    '#required' => TRUE,
  );
  $form['checkout_display'] = array(
    '#type' => 'radios',
    '#title' => t('Payment display'),
    '#options' => array(
      'text' => t('Bitcoin text only'),
      'icon' => t('Bitcoin icon only'),
      'both' => t('Both text and icon'),
    ),
    '#default_value' => $settings['checkout_display'],
    '#description' => t('When selecting a payment option, select the indicator next to the radio buttons for payment options.'),
  );
  $form['returnurl'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['returnurl'],
    '#title' => t('Return URL'),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('URL for redirecting customer to receipt page.'),
    '#required' => FALSE,
  );
  $form['redirect_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Checkout redirect mode'),
    '#options' => array(
      'iframe' => t('Stay on this site using an iframe to embed the hosted checkout page'),
      'hosted' => t('Redirect to a checkout page hosted by BIPS'),
    ),
    '#description' => t('Using the hosted page will redirect to a generically themed page hosted by BIPS.'),
    '#default_value' => $settings['redirect_mode'],
  );
  return $form;
}

/**
 * Payment method callback: submit form submission.
 *
 * Processes payment as necessary using data inputted via the payment details
 * form elements on the form, resulting in the creation of a payment
 * transaction.
 *
 * @param array $payment_method
 *   An array containing BIPS info hook values and user settings.
 */
function commerce_bips_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  // Get order to wrapper.
  $wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Get order price.
  $amount = $wrapper->commerce_order_total->amount->value();

  // @link https://drupal.org/node/2136989 $0 invoice throws an error @endlink
  if (empty($amount)) {
    watchdog('commerce_bips', 'Skipping payment on order @id for zero balance.', array('@id' => $order->order_id), WATCHDOG_INFO, l(t('view order'), 'admin/commerce/orders/' . $order->order_id));
    commerce_checkout_complete($order);
    drupal_goto(_commerce_bips_redirecturl($payment_method, $order));
  }

  $order->data['bips'] = $pane_values;

  commerce_bips_transaction($payment_method, $order, $charge);
}

/**
 * Payment method callback: redirect form.
 *
 * For the hosted checkout page, this form automatically redirects to the
 * BIPS hosted invoice page through an HTTP GET request. For the iframe,
 * this returns form values for displaying markup elements necessary to embed
 * the iframe and a submit button.
 *
 * @param array $form
 *   Probably an empty array when this gets executed.
 * @param array $form_state
 *   Form submission data including order node information and payment method
 *   information.
 * @param object $order
 *   An object of general order information.
 * @param array $payment_method
 *   An array containing payment_method_info hook values and user settings.
 */
function commerce_bips_redirect_form($form, &$form_state, $order, $payment_method) {
  // Get order to wrapper.
  $wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Get order price.
  $amount = $wrapper->commerce_order_total->amount->value();
  $order_total = $wrapper->commerce_order_total->value();

  $transaction_info = db_select('commerce_payment_transaction', 'cpt')
    ->fields('cpt', array('transaction_id', 'remote_id', 'message', 'changed'))
    ->condition('cpt.payment_method', 'bips')
    ->condition('cpt.order_id', $order->order_id)
    ->orderBy('cpt.transaction_id', 'DESC')
    ->range(0, 1)
    ->addTag('commerce_bips_redirect_transaction')
    ->execute()
    ->fetchAssoc();
  if ($transaction_info && is_array($transaction_info)) {
    $transaction = commerce_payment_transaction_load($transaction_info['transaction_id']);
  }
  else {
    watchdog('commerce_bips', 'Failed to load a transaction for order @id.', array('@id' => $order->order_id));
  }

  /*
  Add a huge loop just to list a cropped item on the invoice.
  */
  $item_description = '';
  // Get the products in the order.
  foreach ($order->commerce_line_items as $commerce_line_items) {
    foreach ($commerce_line_items as $line_item) {
      $line_item_description = commerce_line_item_load($line_item['line_item_id']);
      if ($line_item_description->type == 'product') {
        if (!empty($line_item_description->commerce_product['und'][0]['product_id'])) {
          // Load the product title and SKU.
          $product = commerce_product_load($line_item_description->commerce_product['und'][0]['product_id']);
          // Extra trims remove trailing zeros and . if no zeros remain.
          $item_description = trim(trim(trim($line_item_description->quantity, '0'), '.') . 'x ' . $product->title . ' - ' . $product->sku);
        }
        else {
          // Just send the SKU.
          $item_description = trim(trim(trim($line_item_description->quantity, '0'), '.') . 'x ' . $line_item_description->line_item_label);
        }
        if (count($commerce_line_items) > 1) {
          $description_ending = t(', etc.');
        }
        else {
          $description_ending = '';
        }

        // Only describe the first item to BIPS.
        break 2;
      }
    }
  }
  if ((strlen($item_description) + strlen($description_ending)) > 100) {
    $item_description = substr($item_description, 0, (97 - strlen($description_ending))) . $description_ending . '...';
  }
  else {
    $item_description .= $description_ending;
  }

  $site_name = variable_get('site_name', 'Drupal');
  $decimal_amount = commerce_currency_amount_to_decimal($amount, $order_total['currency_code']);

  $ch = curl_init("https://bips.me/api/v1/invoice");
  curl_setopt($ch, CURLOPT_POST, TRUE);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($ch, CURLOPT_USERPWD, $payment_method['settings']['apikey'] . ":");
  curl_setopt($ch, CURLOPT_SSLVERSION,3);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
  curl_setopt($ch, CURLOPT_POSTFIELDS, array(
    'price' => $decimal_amount,
    'currency' => $order_total['currency_code'],
    'item' => $item_description,
    'callbackurl' => url('bips/ipn/' . $order->order_id . '/' . $transaction->transaction_id . '/' . $transaction->data['secret'], array('absolute' => TRUE)),
    // Success and cancel for the hosted invoice to redirect back to Drupal.
    'returnurl' => url('checkout/' . $order->order_id . '/complete', array('absolute' => TRUE)),
    'cancelurl' => url('checkout/' . $order->order_id . '/commerce_bips/cancel_payment', array('absolute' => TRUE)),
  ));
  $invoice_url = curl_exec($ch);

  // Update the order status to the payment redirect page.
  commerce_order_status_update($order, 'checkout_payment', FALSE, NULL, t('Customer clicked the button to pay with Bitcoin on the cart page.'));
  commerce_order_save($order);

  switch ($payment_method['settings']['redirect_mode']) {
    case 'hosted':
      // Redirect to BIPS hosted invoice page.
      drupal_goto($invoice_url);
      break;

    case 'iframe':
      // Display an invoice in an iframe on the confirm payment page.
      $form['#action'] = url('checkout/' . $order->order_id . '/complete', array('absolute' => TRUE));

      $form['iframe'] = array(
        '#markup' => '<iframe seamless style="width:100%; height:260px; overflow:hidden; border:none; margin:auto; display:block;" scrolling="no" allowtransparency="true" frameborder="0" src="' . $invoice_url . '/iframe"><a href="' . $invoice_url . '" target="_blank">Pay now with BIPS</a></iframe>',
      );
      $form['buttons'] = array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('checkout-buttons')),
      );
      $form['buttons']['continue'] = array(
        '#type' => 'button',
        '#value' => t('Complete checkout'),
        '#attributes' => array('class' => array('checkout-continue')),
        '#suffix' => '<span class="checkout-processing element-invisible"></span>',
      );

      $button_operator = '<span class="button-operator">' . t('or') . '</span>';
      $form['buttons']['back'] = array(
        '#prefix' => $button_operator,
        '#markup' => l(t('Go back'), "checkout/$order->order_id/commerce_bips/cancel_payment",
          array(
            'attributes' => array(
              'class' => array('checkout-back'),
              'id' => 'cancel-bips-payment',
            ),
          )
        ),
      );
      return $form;
  }
}

/**
 * Central place to figure out a redirect URL after completed checkout.
 *
 * @return string
 *   A URL to redirect users to after successful checkout.
 */
function _commerce_bips_redirecturl($payment_method, $order) {
  global $base_url;
  if ($payment_method['settings']['returnurl'] == '') {
    return $base_url . '/checkout/' . $order->order_id . '/complete';
  }
  return $payment_method['settings']['returnurl'];
}
